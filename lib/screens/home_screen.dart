import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_list_app/data/data.dart';
import 'package:todo_list_app/model/todo/bloc/bloc.dart';
import 'package:todo_list_app/model/todo/todo.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    // ignore: close_sinks
    final todoBloc = BlocProvider.of<TodoBloc>(context);
    todoBloc..add(LoadAllTodo());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Todo list'),),
      // TODO : Add Bloc logic here
      body: BlocListener<TodoBloc, TodoState>(
        listener: (context, state) {
          if(state is TodoError){
            final snackBar = SnackBar(
              content: Text(state.message),
            );
            Scaffold.of(context).showSnackBar(snackBar);
          }
        },
        child: BlocBuilder<TodoBloc, TodoState>(
          builder: (context, state) {
            if(state is InitialTodoState){
              print('initial sate');
              return buildInitialState();
            } else if(state is LoadedTodosState){
              return buildLoadedTodos(context, state.todos.length);
            } else if(state is TodoError){
              return buildInitialState();
            } else {
              return buildInitialState();
            }
          },
        ),
      ),
    );
  }

  Widget buildInitialState() {
    return Center(child: CircularProgressIndicator());
  }

  Widget buildLoadedTodos(BuildContext context, int todosLength) {
    return ListView(
      children: List.generate(todosLength, (i){
        final Todo todo = todos[i];
        return Card(child: ListTile(
          title: Text(todo.title),
          subtitle: Text(todo.description),
        ),);
      }),
    );
  }
}
