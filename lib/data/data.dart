import 'package:todo_list_app/model/todo/todo.dart';

List<Todo> todos = [
  Todo(
    id: 1,
      title: 'Children',
      description: 'Get the children from the school at 1:00pm'
  ),
  Todo(
      id: 2,
      title: 'Work',
      description: 'Create an awesome Flutter app !'
  ),
  Todo(
      id: 3,
      title: 'Sport',
      description: 'Make 120 basketball shots from middle range'
  ),
];
