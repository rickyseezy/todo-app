import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:todo_list_app/model/todo/repository/todo_repository.dart';
import 'package:todo_list_app/model/todo/todo.dart';

import 'bloc.dart';

class TodoBloc extends Bloc<TodoEvent, TodoState> {
  final ITodoRepository todoRepository;

  TodoBloc(this.todoRepository);

  @override
  TodoState get initialState => InitialTodoState();

  @override
  Stream<TodoState> mapEventToState(
    TodoEvent event,
  ) async* {
    // TODO: Add Logic
    if (event is LoadAllTodo) {
      yield* _mapLoadedTodosToState();
    } else if (event is LoadTodo) {
      yield* _mapLoadedTodoToState(event);
    } else if (event is AddTodo) {
      yield* _mapAddTodoToState(event);
    } else if (event is DeleteTodo) {
      yield* _mapDeleteTodoToState(event);
    } else if (event is UpdateTodo) {
      yield* _mapUpdateTodoToState(event);
    }
  }

  Stream<TodoState> _mapLoadedTodosToState() async* {
    try {
      final todos = await todoRepository.fetchAllTodo();
      yield LoadedTodosState(todos);
    } on NotFoundError {
      yield TodoError("Something wrong happened, try later ...");
    } on NetworkError {
      yield TodoError("Is the device connected to internet ?");
    }
  }

  Stream<TodoState> _mapLoadedTodoToState(LoadTodo event) async* {
    try {
      final todo = await todoRepository.fetchOneTodo(event.id);
      yield LoadedSingleTodoState(todo);
    } on NotFoundError {
      yield TodoError("Something wrong happened, try later ...");
    } on NetworkError {
      yield TodoError("Is the device connected to internet ?");
    }
  }

  Stream<TodoState> _mapAddTodoToState(AddTodo event) async* {
    try {
      await todoRepository.addTodo(
          title: event.title,
          description: event.description,
          complete: event.completed);
      final todos = await todoRepository.fetchAllTodo();

      yield LoadedTodosState(todos);
    } on NotFoundError {
      yield TodoError("Something wrong happened, try later ...");
    } on NetworkError {
      yield TodoError("Is the device connected to internet ?");
    }
  }

  Stream<TodoState> _mapDeleteTodoToState(DeleteTodo event) async* {
    try {
      await todoRepository.deleteTodo(event.todo);
      final todos = await todoRepository.fetchAllTodo();
      yield LoadedTodosState(todos);
    } on NotFoundError {
      yield TodoError("Something wrong happened, try later ...");
    } on NetworkError {
      yield TodoError("Is the device connected to internet ?");
    }
  }

  Stream<TodoState> _mapUpdateTodoToState(UpdateTodo event) async* {
    try {
      final todo = Todo(
          title: event.title,
          description: event.description,
          complete: event.complete);

      await todoRepository.updateTodo(todo);
    } on NotFoundError {
      yield TodoError("Something wrong happened, try later ...");
    } on NetworkError {
      yield TodoError("Is the device connected to internet ?");
    }
  }
}
