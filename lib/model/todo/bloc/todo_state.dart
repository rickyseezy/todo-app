import 'package:equatable/equatable.dart';
import 'package:todo_list_app/model/todo/todo.dart';

abstract class TodoState extends Equatable {
  const TodoState();
}

class InitialTodoState extends TodoState {
  @override
  List<Object> get props => [];
}

class LoadedTodosState extends TodoState {
  final List<Todo> todos;

  LoadedTodosState(this.todos);

  @override
  List<Object> get props => [todos];
}

class LoadedSingleTodoState extends TodoState {
  final Todo todo;

  LoadedSingleTodoState(this.todo);

  @override
  List<Object> get props => [todo];
}

class TodoError extends TodoState {
  final String message;

  TodoError(this.message);

  @override
  List<Object> get props => [message];
}