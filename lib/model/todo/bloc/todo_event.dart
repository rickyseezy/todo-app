import 'package:equatable/equatable.dart';
import 'package:todo_list_app/model/todo/todo.dart';

abstract class TodoEvent extends Equatable {
  const TodoEvent();
}

class LoadAllTodo extends TodoEvent {

  @override
  List<Object> get props => [];
}

class LoadTodo extends TodoEvent {
  final int id;

  const LoadTodo(this.id);

  @override
  List<Object> get props => [id];
}

class AddTodo extends TodoEvent {
  final String title;
  final String description;
  final bool completed;

  const AddTodo(this.title, this.description, {this.completed = false});

  @override
  List<Object> get props => [title, description, completed];
}

class DeleteTodo extends TodoEvent {
  final Todo todo;

  const DeleteTodo(this.todo);

  @override
  List<Object> get props => [todo];
}

class UpdateTodo extends TodoEvent {
  final Todo todo;
  final String title;
  final String description;
  final bool complete;

  const UpdateTodo(this.todo, this.title, this.description, this.complete);

  @override
  List<Object> get props => [todo, title, description, complete];
}