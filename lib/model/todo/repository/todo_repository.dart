
import 'dart:math';

import 'package:todo_list_app/data/data.dart';
import 'package:todo_list_app/model/todo/todo.dart';

abstract class ITodoRepository {
  Future<List<Todo>> fetchAllTodo();
  Future<Todo> fetchOneTodo(int id);
  Future<Todo> addTodo({String title, String description, bool complete});
  Future<Todo> updateTodo(Todo todo);
  Future<void> deleteTodo(Todo todo);
}

class TodoRepository implements ITodoRepository {

  @override
  Future<List<Todo>> fetchAllTodo() {
    return Future.delayed(Duration(seconds: 2), (){
      /*final random = Random();

      if(random.nextBool()){
        throw NetworkError();
      }
*/
      return [...todos];
    });
  }

  @override
  Future<Todo> fetchOneTodo(int id) {
    return Future.delayed(Duration(seconds: 4), (){

      final random = Random();

      if(random.nextBool()){
        throw NetworkError();
      }

      if(todos.any((todo) => todo.id == id)){
        return todos.firstWhere((todo) => todo.id == id);
      }
      throw NotFoundError();
    });
  }

  @override
  Future<void> deleteTodo(Todo todo) {
    return Future.delayed(Duration(seconds: 3), (){
      if(!todos.contains(todo)){
        throw NotFoundError();
      }
      todos.remove(todo);
    });
  }

  @override
  Future<Todo> updateTodo(Todo todo, {String title = '', String description = '', bool complete = false}) {
    return Future.delayed(Duration(seconds: 2), (){
      if(todos.any((fetchedTodo) => fetchedTodo.id == todo.id)){
        Todo fetchedTodo = todos.firstWhere((res) => res.id == todo.id);
        todos.remove(fetchedTodo);
      }

      final generatedId = todos.length++;

      final updatedTodo = Todo(id: generatedId, title: title, description: description, complete: complete);
      todos.add(updatedTodo);

      return updatedTodo;
    });
  }

  @override
  Future<Todo> addTodo({String title = '', String description = '', bool complete = false}) {
    return Future.delayed(Duration(seconds: 1), (){
      final int id = todos.length++;
      final String newTitle = title;
      final String newDescription = description;
      Todo todo = Todo(id: id, title: newTitle, description: newDescription, complete: complete);
      todos.add(todo);
      return todo;
    });
  }
}

class NotFoundError extends Error {}

class NetworkError extends Error {}