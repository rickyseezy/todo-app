import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Todo extends Equatable {
  final int id;
  final String title;
  final String description;
  final bool complete;

  Todo({this.id, @required this.title, @required this.description, this.complete = false});

  @override
  List<Object> get props => [id, title, description, complete];

}
